#include "intrins.h"
#include "zf_gpio.h"
#include "bldc_config.h"
#include "comparator.h"
#include "pwm.h"




//-------------------------------------------------------------------------------------------------------------------
//  @brief      软延时
//  @param      void                        
//  @return     void          
//  @since      v1.0
//  Sample usage:
//-------------------------------------------------------------------------------------------------------------------
void delay_500ns(void)
{
	_nop_();
    _nop_();
    _nop_();
    _nop_();
    _nop_();
    _nop_();
    _nop_();
    _nop_();
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      刹车（关闭输出并开启所有下桥）
//  @param      void                        
//  @return     void          
//  @since      v1.0
//  Sample usage:
//-------------------------------------------------------------------------------------------------------------------
void pwm_brake(void)
{
    PWMA_ENO = 0;
    PWM_A_L_PIN = 1;
    PWM_B_L_PIN = 1;
    PWM_C_L_PIN = 1;
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      关闭输出
//  @param      void                        
//  @return     void          
//  @since      v1.0
//  Sample usage:
//-------------------------------------------------------------------------------------------------------------------
void pwm_close_output(void)
{
    PWM_A_L_PIN = 0;
    PWM_B_L_PIN = 0;
    PWM_C_L_PIN = 0;
    PWMA_ENO = 0;
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      开启A上B下
//  @param      void                        
//  @return     void          
//  @since      v1.0
//  Sample usage:
//-------------------------------------------------------------------------------------------------------------------
void pwm_a_bn_output(void)
{
    pwm_close_output();
    delay_500ns();
    PWMA_ENO = 1<<0;
    PWM_B_L_PIN = 1;
    comparator_select_c();
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      开启A上C下
//  @param      void                        
//  @return     void          
//  @since      v1.0
//  Sample usage:
//-------------------------------------------------------------------------------------------------------------------
void pwm_a_cn_output(void)
{
    pwm_close_output();
    delay_500ns();
    PWMA_ENO = 1<<0;
    PWM_C_L_PIN = 1;
    comparator_select_b();
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      开启B上C下
//  @param      void                        
//  @return     void          
//  @since      v1.0
//  Sample usage:
//-------------------------------------------------------------------------------------------------------------------
void pwm_b_cn_output(void)
{
    pwm_close_output();
    delay_500ns();
    PWMA_ENO = 1<<2;
    PWM_C_L_PIN = 1;
    comparator_select_a();
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      开启B上A下
//  @param      void                        
//  @return     void          
//  @since      v1.0
//  Sample usage:
//-------------------------------------------------------------------------------------------------------------------
void pwm_b_an_output(void)
{
    pwm_close_output();
    delay_500ns();
    PWMA_ENO = 1<<2;
    PWM_A_L_PIN = 1;
    comparator_select_c();
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      开启C上A下
//  @param      void                        
//  @return     void          
//  @since      v1.0
//  Sample usage:
//-------------------------------------------------------------------------------------------------------------------
void pwm_c_an_output(void)
{
    pwm_close_output();
    delay_500ns();
    PWMA_ENO = 1<<4;
    PWM_A_L_PIN = 1;
    comparator_select_b();
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      开启C上B下
//  @param      void                        
//  @return     void          
//  @since      v1.0
//  Sample usage:
//-------------------------------------------------------------------------------------------------------------------
void pwm_c_bn_output(void)
{
    pwm_close_output();
    delay_500ns();
    PWMA_ENO = 1<<4;
    PWM_B_L_PIN = 1;
    comparator_select_a();
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      关闭PWM中断
//  @param      void                        
//  @return     void          
//  @since      v1.0
//  Sample usage:
//-------------------------------------------------------------------------------------------------------------------
void pwm_isr_close(void)
{
    PWMA_IER = 0;
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      开启PWM中断
//  @param      void                        
//  @return     void          
//  @since      v1.0
//  Sample usage:
//-------------------------------------------------------------------------------------------------------------------
void pwm_isr_open(void)
{
    PWMA_IER = 0x10;
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      更新PWM占空比
//  @param      void                        
//  @return     void          
//  @since      v1.0
//  Sample usage:
//-------------------------------------------------------------------------------------------------------------------
void pwm_center_duty_update(uint16 duty)
{
    uint8 temp_h, temp_l;
    
    duty = BLDC_MAX_DUTY - duty;
    
    temp_h = duty >> 8;
    temp_l = (uint8)duty;
    
    PWMA_CCR1H = temp_h;
    PWMA_CCR1L = temp_l;
    
    PWMA_CCR2H = temp_h;
    PWMA_CCR2L = temp_l;
    
    PWMA_CCR3H = temp_h;
    PWMA_CCR3L = temp_l;
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      PWM初始化（中心对齐方式）
//  @param      void                        
//  @return     void          
//  @since      v1.0
//  Sample usage:
//-------------------------------------------------------------------------------------------------------------------
void pwm_center_init(void)
{
    PWM_A_H_PIN = 0;
	PWM_A_L_PIN = 0;
	PWM_B_H_PIN = 0;
	PWM_B_L_PIN = 0;
	PWM_C_H_PIN = 0;
	PWM_C_L_PIN = 0;
    
    // 初始化低边为推挽输出
    gpio_mode(P2_0, GPO_PP);
    gpio_mode(P2_1, GPO_PP);
    gpio_mode(P2_2, GPO_PP);
    gpio_mode(P2_3, GPO_PP);
    gpio_mode(P2_4, GPO_PP);
    gpio_mode(P2_5, GPO_PP);
    

	PWMA_CCER1  = 0;
	PWMA_CCER2  = 0;
	PWMA_SR1    = 0;
	PWMA_SR2    = 0;
    PWMA_ENO    = 0;
	PWMA_IER    = 0;
    
    // 设置PWM引脚
    PWMA_PS     = 0x55;
    
    PWMA_CCMR1  = 0x78;		// 通道模式配置, PWM模式2, 预装载允许
	PWMA_CCR1H  = (uint8)(BLDC_MAX_DUTY >> 8);		// 比较值, 控制占空比(高电平时钟数)
    PWMA_CCR1L  = (uint8)(BLDC_MAX_DUTY & 0xff);
	PWMA_CCER1 |= 0x01;		// 开启比较输出, 高电平有效
    
	PWMA_CCMR2  = 0x78;		// 通道模式配置, PWM模式1, 预装载允许
	PWMA_CCR2H  = (uint8)(BLDC_MAX_DUTY >> 8);		// 比较值, 控制占空比(高电平时钟数)
    PWMA_CCR2L  = (uint8)(BLDC_MAX_DUTY & 0xff);
	PWMA_CCER1 |= 0x10;		// 开启比较输出, 高电平有效
    
    PWMA_CCMR3  = 0x78;		// 通道模式配置, PWM模式1, 预装载允许
	PWMA_CCR3H  = (uint8)(BLDC_MAX_DUTY >> 8);		// 比较值, 控制占空比(高电平时钟数)
    PWMA_CCR3L  = (uint8)(BLDC_MAX_DUTY & 0xff);
	PWMA_CCER2 |= 0x01;		// 开启比较输出, 高电平有效
    
    PWMA_CCMR4  = 0x78;		// 通道模式配置, PWM模式1, 预装载允许
    PWMA_CCR4H  = (uint8)((BLDC_MAX_DUTY-1) >> 8);	// 比较值, 控制占空比(高电平时钟数)
    PWMA_CCR4L  = (uint8)((BLDC_MAX_DUTY-1) & 0xff);

    PWMA_IER    = 0x10;     // 开启更新中
    //PWMA_ENO = 0xff;//0X15;
    
    // 预分频
    PWMA_PSCRH = 0;
    PWMA_PSCRL = 0;
    
    // 设置周期
    PWMA_ARRH = (uint8)((BLDC_MAX_DUTY-1) >> 8);
    PWMA_ARRL = (uint8)((BLDC_MAX_DUTY-1) & 0xff);
    
    PWMA_BKR    = 0x80;		// 主输出使能 相当于总开关
	PWMA_CR1    = 0xA5;		// 使能计数器, 允许自动重装载寄存器缓冲, 中央对齐模式, 向上计数, 只有计数器上下溢出才触发更新中断,  bit7=1:写自动重装载寄存器缓冲(本周期不会被打扰), =0:直接写自动重装载寄存器本(周期可能会乱掉)
	PWMA_EGR    = 0x01;		// 产生一次更新事件, 清除计数器和与分频计数器, 装载预分频寄存器的值
    
    // 刚开始使用刹车将
    PWM_A_L_PIN = 1;
    PWM_B_L_PIN = 1;
    PWM_C_L_PIN = 1;
}
